# frozen_string_literal: true

require_relative 'document'

# Unit Tests for the Document class
RSpec.describe Document do
  before do
    @content = 'A bunch of words'
    @doc = Document.new('title', 'author', @content)
  end

  it 'should hold on to the contents' do
    expect(@doc.content).to eq(@content)
  end

  it 'should return all of the words in the document' do
    expect(@doc.words).to eq(@content.split)
  end

  it 'should know how many words it contains' do
    expect(@doc.word_count).to eq(4)
  end
end
