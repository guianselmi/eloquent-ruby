# frozen_string_literal: true

require 'test/unit'
require_relative 'document'

# Tests for the Document class using Test::Unit
class DocumentTest < Test::Unit::TestCase
  def setup
    @content = 'A bunch of words'
    @doc = Document.new('title', 'author', @content)
  end

  def test_that_document_holds_onto_contents
    assert_equal @content, @doc.content, 'Contents are still there.'
  end

  def test_that_doc_can_return_words_in_array
    assert @doc.words.include?('A')
    assert @doc.words.include?('bunch')
    assert @doc.words.include?('of')
    assert @doc.words.include?('words')
  end

  def test_that_word_count_is_correct
    assert_equal 4, @doc.word_count, 'Word count is correct.'
  end
end
