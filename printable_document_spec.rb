# frozen_string_literal: true

require_relative 'printable_document'

# Unit Tests for the PrintableDocument class
RSpec.describe PrintableDocument do
  before do
    @content = 'A bunch of words'
    @doc = PrintableDocument.new('title', 'author', @content)
    @double_printer = double
  end

  it 'should know how to print itself' do
    allow(@double_printer).to receive_messages(available?: true, render: nil)
    expect(@doc.print(@double_printer)).to eq('Done')
  end

  it 'should return the proper string if printer is offline' do
    allow(@double_printer).to receive_messages(available?: false, render: nil)
    expect(@doc.print(@double_printer)).to eq('Printer unavailable')
  end
end
