# frozen_string_literal: true

# Creates a Title
class Title
  attr_reader :long_title, :short_title, :isbn

  def initialize(long_title, short_title, isbn)
    @long_title = long_title
    @short_title = short_title
    @isbn = isbn
  end
end
