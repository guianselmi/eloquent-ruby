# ELOQUENT RUBY

Project created while I studied the **Ruby** programming language and read one of the most recommended books in that regard: [_ELOQUENT RUBY_, by Russ Olsen](http://eloquentruby.com/).

The book is still relevant despite its age.

Therefore, part of my study was to adapt the outdated code and unit tests. For that, I used Ruby 3.x and the latest RSpec, making the code up to date.
