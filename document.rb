# frozen_string_literal: true

# Creates a document with attributes that are expected on a Document.
# Rubocop threatened me until I added this comment. Send help.
class Document
  attr_accessor :title
  attr_reader :author, :content

  def initialize(title, author, content)
    @title = title
    @author = author
    @content = content
  end

  def about_me
    puts "I am #{self}"
    puts "My title is #{title}"
    puts "I have #{word_count} words"
  end

  def to_s
    "Document: #{title.long_title}, by #{author.last_name}."
  end

  def words
    content.split
  end

  def word_count
    words.size
  end
end
